# TODO
- Pages
    - log in page
    - game page
    - player page
    - inventory page
- Design
    - sign up process
    - log in process
    - depth currency
    - secondary currency
        - Decide on secondary currency name
- API
    - Add versioning
    - Sign up
    - Log in
    - Dig
        - different metrics
            - Don't worry about handling obscuring and verification for depth
    - Buying 
        - Essentially the same operation as digging but need add item purchase


# Finished
- Mock-up django workflow
- Mock-up frontend workflow
- Hook up react router
- Create home page
- Create sign up page