# Environment Configuration
- brew install mysql
- pip install django
- pip install mysqlclient

# Django Documenation
- Understanding Django Views
    - https://docs.djangoproject.com/en/2.0/topics/class-based-views/intro/

# Django REST Framework Documentation
- Generic Views (the stuff that rubber stamps common operations)
    - http://www.django-rest-framework.org/api-guide/generic-views/#generic-views

# Tutorial Documentation
- HOW TO understand what you're getting into IN ORDER TO set expectations
    - http://polyglot.ninja/django-building-rest-apis/
- HOW TO Make the basic Django project and architecture IN ORDER TO have a reliable environment
    - http://polyglot.ninja/django-building-rest-apis/
- HOW TO Handle requests with django rest framework IN ORDER TO render views, and respond to requests
    - http://polyglot.ninja/django-rest-framework-getting-started/
- HOW TO serialize data IN ORDER TO simplify validation of REST arguments
    - http://polyglot.ninja/django-rest-framework-serializers/
- HOW TO use pre-built templates as views IN ORDER TO rubber stamp endpoint development for common operations
    - http://polyglot.ninja/django-rest-framework-modelserializer-generic-views/
- HOW TO...
    - http://polyglot.ninja/django-rest-framework-viewset-modelviewset-router/
- HOW TO...
    - http://polyglot.ninja/django-rest-framework-authentication-permissions/
- HOW TO best practices IN ORDER TO be more idiomatic
    - http://polyglot.ninja/rest-api-best-practices-python-flask-tutorial/

# Django Authentication and Authorization
- Using Serializer to create a user
    - https://stackoverflow.com/a/43032332
- Django login_required decorator for blocking people from routes
    - https://docs.djangoproject.com/en/2.0/topics/auth/default/#the-login-required-decorator

# Database Documentation
- Database Field Types (For serializers)
    - https://docs.djangoproject.com/en/2.0/ref/models/fields/#field-types

# ViewSet
- list – list all elements, serves GET to /api/subscriber
- create – create a new element, serves POST to /api/subscriber
- retrieve – retrieves one element, serves GET to /api/subscriber/1
- update and partial_update – updates single element, handles PUT/PATCH to /api/subscriber/1
- destroy – deletes single element, handles DELETE to /api/subscriber/1

# Stretch Goals
- HOW TO make a custom user model for Django IN ORDER TO not be constrained if I need to do something crazy
    - https://www.codingforentrepreneurs.com/blog/how-to-create-a-custom-django-user-model/

# JWT + React Tutorial
- https://medium.com/netscape/full-stack-django-quick-start-with-jwt-auth-and-react-redux-part-i-37853685ab57

# HTTP Status Codes
- https://en.wikipedia.org/wiki/List_of_HTTP_status_codes