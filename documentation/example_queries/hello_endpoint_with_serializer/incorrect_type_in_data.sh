# Age must be an integer, or able to be coerced into an integer
curl -H "Content-Type: application/json" \
     -X POST \
     -d '{"name": "masnun", "age": "ten"}' \
     --url http://localhost:8000/api/hello