# Python Standard Libraries
# N/A
# Third-Party Libraries
from django.conf.urls import url
from django.views import generic
# Custom Libraries
from .views import example
from .views import home_page

app_name = "webpage"

urlpatterns = []
urlpatterns += [
    # url(r"^$", home_page, name="home_page"),
    url(r"^$", example, name="example"),
    # Catch all for everything, but I think this will gobble other
    # See: https://docs.djangoproject.com/en/2.0/ref/class-based-views/base/#redirectview
    # url(r".*", generic.RedirectView.as_view(url="/",
    #                                         permanent=False)),
]
