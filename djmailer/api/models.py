# Python Standard Libraries
# N/A
# Third-Party Libraries
from django.db import models
# Custom Libraries
# N/A


class Subscriber(models.Model):
    """TODO."""
    name = models.CharField("Name", max_length=50)
    age = models.IntegerField("Age", default=10)
    email = models.EmailField("Email")
