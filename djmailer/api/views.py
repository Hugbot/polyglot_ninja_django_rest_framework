# Python Standard Libraries
# N/A
# Third-Party Libraries
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.db import IntegrityError
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view
from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.generics import ListCreateAPIView
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED
from rest_framework.status import HTTP_401_UNAUTHORIZED
from rest_framework.status import HTTP_400_BAD_REQUEST
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
# Custom Libraries
from .serializers import HelloWorldSerializer
from .serializers import SubscriberSerializer
from .serializers import UserSerializer
from .models import Subscriber


class HelloWorldView(APIView):

    def get(self, request):
        all_subscribers = Subscriber.objects.all()
        serialized_subscribers = SubscriberSerializer(all_subscribers, many=True)
        return Response(serialized_subscribers.data)

    def post(self, request):
        serializer = HelloWorldSerializer(data=request.data)
        if serializer.is_valid():
            valid_data = serializer.data

            name = valid_data.get("name")
            age = valid_data.get("age")

            response_data = {
                "message": "Hello {}, you're {} years old".format(name, age)
            }
            return Response(response_data)
        else:
            response_data = {
                "errors": serializer.errors
            }
            return Response(response_data)

# ##############################################################################
# ##########################
# Initial Instance
# ##########################
# class SubscriberView(APIView):
#     def get(self, request):
#         return Response({"message": "Hello World!"})

#     def post(self, request):
#         serializer = SubscriberSerializer(data=request.data)

#         if serializer.is_valid():
#             subscriber_instance = Subscriber.objects.create(**serializer.data)

#             response_data = {
#                 "message": "Created subscriber {}".format(subscriber_instance.id)
#             }

#             return Response(response_data)
#         else:
#             return Response({"errors": serializer.errors})


# ##########################
# Better Instance
# ##########################
# class SubscriberView(ListAPIView, CreateAPIView):
#     serializer_class = SubscriberSerializer
#     queryset = Subscriber.objects.all()


# ##########################
# Best Instance
# ##########################
class SubscriberView(ListCreateAPIView):
    serializer_class = SubscriberSerializer
    queryset = Subscriber.objects.all()


# ##########################
# Brain Melt Instance
# ##########################
class SubscriberViewSet(ModelViewSet):
    serializer_class = SubscriberSerializer
    queryset = Subscriber.objects.all()
# ##############################################################################


@api_view(["POST"])
def login(request):
    username = request.data.get("username")
    password = request.data.get("password")

    user = authenticate(username=username, password=password)
    if not user:
        return Response({"error": "Login failed"},
                        status=HTTP_401_UNAUTHORIZED)

    token, _ = Token.objects.get_or_create(user=user)
    return Response({"token": token.key})


@api_view(["POST"])
def signup(request):
    user_serializer = UserSerializer(data=request.data)

    if user_serializer.is_valid():
        try:
            user_serializer.save()

            # status_message = "User {} was successfully created.".format(user.name)
            status_message = "User was successfully created."
            response_data = {"status": status_message}

            return Response(response_data,
                            status=HTTP_201_CREATED)
        except IntegrityError:
            # user already exists
            status_message = 'user already exists'
            response_data = {"errors": status_message}

            return Response(response_data,
                            status=HTTP_401_UNAUTHORIZED)
    else:
        response_data = {"errors": user_serializer.errors}
        return Response(response_data,
                        status=HTTP_401_UNAUTHORIZED)
