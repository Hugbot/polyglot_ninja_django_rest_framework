# Python Standard Libraries
# N/A
# Third-Party Libraries
from django.conf.urls import url
from rest_framework.routers import SimpleRouter
from rest_framework.schemas import get_schema_view
from rest_framework_simplejwt.views import (TokenObtainPairView,
                                            TokenRefreshView, )
# Custom Libraries
from .views import HelloWorldView
from .views import SubscriberView
from .views import SubscriberViewSet
from .views import login
from .views import signup

app_name = "api"

hello_world_view = HelloWorldView()

router = SimpleRouter()
router.register("subscribers", SubscriberViewSet)

urlpatterns = []

urlpatterns += router.urls

urlpatterns += [
    url(r'^$', get_schema_view()),
    url(r"^hello", hello_world_view.as_view(), name="hello_world"),
    url(r"^login", login),
    url(r"^signup", signup),
    # url(r"^subscriber", SubscriberView.as_view(), name="subscriber")
    # For: /api/subscriber
    # url(r"^subscriber",
    #     SubscriberViewSet.as_view({"get": "list", "post": "create"}),
    #     name="subscriber")
    # # For: /api/subscriber/1
    # url(r"^subscriber",
    #     SubscriberViewSet.as_view({"get": "retrieve", "put": "update"}),
    #     name="subscriber")
    # ----------
    # Separate tutorial
    # ----------
    # url(r'^api/auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^auth/token/obtain/$', TokenObtainPairView.as_view()),
    url(r'^auth/token/refresh/$', TokenRefreshView.as_view()),
]
