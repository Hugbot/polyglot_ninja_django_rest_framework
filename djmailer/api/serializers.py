# Python Standard Libraries
# N/A
# Third-Party Libraries
from django.contrib.auth.models import User
from rest_framework import serializers
# Custom Libraries
from .models import Subscriber


class HelloWorldSerializer(serializers.Serializer):
    """TODO."""
    name = serializers.CharField(required=True, max_length=6)
    age = serializers.IntegerField(required=False, min_value=10, default=10)
    email = serializers.EmailField()


# ##########################
# Initial instance
# ##########################
# class SubscriberSerializer(serializers.Serializer):
#     name = serializers.CharField(max_length=50)
#     age = serializers.IntegerField()
#     email = serializers.EmailField()


# ##########################
# Better instance
# ##########################
class SubscriberSerializer(serializers.ModelSerializer):

    class Meta:
        model = Subscriber
        fields = "__all__"


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=20)

    class Meta:
        model = User
        fields = "__all__"

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()

        return user
